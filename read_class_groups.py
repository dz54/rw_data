def read_json(infile):
    """read class groups from JSON file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import json
    with open(infile, 'r') as f:
        class_groups = json.load(f)

    return class_groups


def read_csv(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import csv
    with open(infile, 'r') as f:
        c = csv.reader(infile)
        # FIX ME!!
        class_groups = {}
        for r in c:
            class_groups[r[0]] = r[1]

    return class_groups


def read_csv_numpy(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    from numpy import loadtxt
    # COMPLETE ME!!
    class_groups = loadtxt(infile);

    return class_groups
